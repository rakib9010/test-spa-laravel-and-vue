<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        UUID, name, description, code and status (active and inactive)
        Schema::create('records', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id')->index('user_id');
            $table->string('uuid')->index('uuid');
            $table->string('name')->index('name');
            $table->string('code')->index('code');
            $table->text('description');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('records');
    }
}
