<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/user/add', [\App\Http\Controllers\Api\UserController::class, 'add']);
Route::post('/auth/login', [\App\Http\Controllers\Api\AuthController::class, 'login']);


Route::middleware(['auth:api'])->group(function () {
    Route::post('/auth/logout', [\App\Http\Controllers\Api\AuthController::class, 'logout']);
    Route::post('/auth/me', [\App\Http\Controllers\Api\AuthController::class, 'me']);
    Route::get('/record/get', [\App\Http\Controllers\Api\RecorsController::class, 'getRecords']);
    Route::post('/record/add', [\App\Http\Controllers\Api\RecorsController::class, 'addRecord']);
    Route::put('/record/edit/{record}', [\App\Http\Controllers\Api\RecorsController::class, 'editRecord'])->name('api.edit_record');
    Route::delete('/record/delete/{record}', [\App\Http\Controllers\Api\RecorsController::class, 'deleteRecord'])->name('api.delete_record');
});

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
