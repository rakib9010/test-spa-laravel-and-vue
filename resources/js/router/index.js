import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store"

Vue.use(VueRouter);

import List from "../components/List";
import Login from "../components/Login";
import Register from "../components/Register";
import NotFound from "../components/NotFount";

const router = new VueRouter({
    mode: 'history',
    routes: [
        // dynamic segments start with a colon
        {
            path: '/',
            name: "list",
            component: List,
            meta: {isSecure: true}
        },
        {
            path: '/login',
            name: "login",
            component: Login
        },
        {
            path: '/register',
            name: "register",
            component: Register
        },
        { path: '/*', component: NotFound }

    ]
})


let jwt = JSON.parse( window.localStorage.getItem('jwt') )
if(jwt!=null && typeof jwt.expireTime != 'undefined' && jwt.expireTime > (new Date().getTime())){
    store.dispatch('RESTORE_TOKEN_FROM_LOCALSTORE');
}

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.isSecure && record.meta.isSecure==true)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if (!store.state.isLogedIn || !store.state.token || store.state.token.expireTime < (new Date().getTime())) {
            store.dispatch('LOGOUT');
            // next({
            //     path: '/login',
            //     query: { redirect: to.fullPath }
            // })
        } else {
            next()
        }
    } else {
        if (['login', 'register'].includes(to.name) && store.state.isLogedIn)
            next({ name: 'list' })
        else
            next() // make sure to always call next()!
    }
})

export default router;
