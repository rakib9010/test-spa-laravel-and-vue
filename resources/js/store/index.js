import Vue from "vue"
import Vuex from "vuex"
import router from '../router'
import Api from '../service/api';


Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        isLogedIn : false,
        token : null,
        user: null,
        message : {}
    },
    mutations: {
        LOGIN (state, payload) {

            state.user = null
            payload.expireTime = new Date().getTime() + (payload.expires_in * 1000)
            state.isLogedIn = true
            state.token = payload
            window.localStorage.setItem('jwt', JSON.stringify(state.token))
        },
        SET_USER (state, payload) {
            state.user = payload
            window.localStorage.setItem('user', JSON.stringify(state.user))
        },
        LOGOUT (state, payload) {
            state.isLogedIn = false
            state.user = null
            state.token = null
            window.localStorage.removeItem('jwt')
            window.localStorage.removeItem('user')
        },
        REFRESH_TOKEN (state, payload) {
            state.isLogedIn = true
            state.token = payload
            window.localStorage.setItem('jwt', JSON.stringify(state.token))
        },
        RESTORE_TOKEN_FROM_LOCALSTORE (state) {
            let jwt = JSON.parse( window.localStorage.getItem('jwt') )
            let user = JSON.parse( window.localStorage.getItem('user') )

            if(jwt!=null && typeof jwt.expireTime != 'undefined' && jwt.expireTime > (new Date().getTime() + (300 * 1000))){

                state.isLogedIn = true
                state.token = jwt
                state.user = user

            } else {
                state.isLogedIn = false
                state.user = null
                state.token = null
                window.localStorage.removeItem('jwt')
                window.localStorage.removeItem('user')
            }
        }
    },


    actions: {
        REGISTER ({commit, state}, payload=null) {
            return new Promise((resolve, reject) => {
                try{
                    Api().post('/user/add', payload,{headers: {'Content-Type': 'multipart/form-data'}})
                        .then(
                            (response) => {
                                if(typeof response.status!='undefined' && response.status == 200){
                                    resolve(true)
                                } else {
                                    reject(response)
                                }
                            }
                        )
                        .catch( e => {
                            reject(e.response);
                        });


                } catch (e) {
                    reject(e)
                }
            })
        },

        LOGIN ({commit, state, dispatch}, payload=null) {
            return new Promise((resolve, reject) => {
                try{
                    Api().post('/auth/login', payload)
                        .then(
                            (response) => {
                                if(typeof response.status!='undefined' && response.status == 200){
                                    commit('LOGIN', response.data)


                                    resolve(true)
                                } else {
                                    reject(response)
                                }
                            }
                        ).catch( e => {
                            reject(e.response);
                        });


                } catch (e) {
                    reject(e)
                }
            })
        },

        SET_USER ({commit, state}, payload=null) {
            return new Promise((resolve, reject) => {
                try{
                    let token = state.token!=null ? state.token.access_token : null;
                    token = payload!=null ? payload.access_token : token;
                    Api().post('/auth/me', payload, {headers: {'Authorization': 'Bearer '+token}})
                        .then(
                            (response) => {
                                if(typeof response.status!='undefined' && response.status == 200){
                                    commit('SET_USER', response.data)
                                    resolve(true)
                                } else {
                                    reject(response)
                                }
                            }
                        )
                        .catch( (e) => {
                            reject(e.getMessage());
                        });


                } catch (e) {
                    reject(e)
                }
            })
        },

        LOGOUT({commit}) {
            commit('LOGOUT')
            router.push({name:'login'})
        },

        RESTORE_TOKEN_FROM_LOCALSTORE({commit}) {
            commit('RESTORE_TOKEN_FROM_LOCALSTORE')
        }
    },

    getters: {
        username (state) {
            if(state.isLogedIn && state.user!=null){
                return state.user.name.substr(0, 3);
            }
            return 'Uknown';
        },

        avater (state) {
            if(state.isLogedIn && state.user != null){
                return process.env.MIX_APP_URL + state.user.image;
            }
            return null;
        },
        getSessionExpireTime(state) {
            return state.token!=null? state.token.expireTime : 0;
        }
    },
    modules: {}
})
