require('./bootstrap');
import Vue from 'vue';
import App from './App.vue';
import router from "./router";
import store from "./store";

import VeeValidate from 'vee-validate';


import { BootstrapVue, IconsPlugin, FormFilePlugin } from 'bootstrap-vue';
// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'


// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(FormFilePlugin)

Vue.config.productionTip = false;

Vue.use(VeeValidate, {
    // This is the default
    // inject: true,
    // // Important to name this something other than 'fields'
    fieldsBagName: 'veeFields',
    // // This is not required but avoids possible naming conflicts
    // errorBagName: 'veeErrors'
})


new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')

