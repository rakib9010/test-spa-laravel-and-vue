import axios from "axios";

export default () => {
    return axios.create({
        // baseURL: "http://localhost:8000/api",
        baseURL: process.env.MIX_APP_URL+"/api",
        withCredentials: false,
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        }
    });
}

