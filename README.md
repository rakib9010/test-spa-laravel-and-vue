# Test SPA - Laravel and Vue 
## All the common things that we need to create all the time for any web application are demonstrated in this application.

### Features demonstrated:
- **Create REST API**
- **DB migration**
- **Import .env settings to vue environment**
- **User registration**
- **User sign-in**
- **User auththentication with JWT token**
- **Refresh token in every interval**
- **Fetch logedin user by token**
- **Show logedin user information**
- **Logout**
- **Authorization**
- **CURD operation**
- **Form submit with File Upload (multi-part via ajax)**
- **Preview selected image instantly**
- **Client side form validate**
- **Server side form validate and show error message properly (via ajax)**
- **Create List page**
- **Create pagination**
- **Fetch data with search qery**
- **Create action specific Request object**
- **Segregate data validation and authorization logic from controller action**
- **Implemented policies and Gates to authorize user access to a piece data**
- **Restrict access to specific page from vue router**
- **Transfer all the request to vue router**
- **Used vuex state**
- **Created delete confirmation dialog**
- **Vue component**
- **Data transfered between Vue components**
- **Restore authenticated user when page refresh**
- **Used Bootstrup-vue**
- **Modal created**




### Registration Page
![Alt text](images/register.png?raw=true "Title")

---

### Front side validation
![Alt text](images/2.png?raw=true "Title")

---

### Preview selected image instantly
![Alt text](images/2021-06-10_15-31.png?raw=true "Title")

---

### Show success message
![Alt text](images/2021-06-10_15-32.png?raw=true "Title")

---

### Show server side validation errors
![Alt text](images/2021-06-10_15-35.png?raw=true "Title")
![Alt text](images/2021-06-10_15-38.png?raw=true "Title")

---

### Login page
![Alt text](images/2021-06-10_15-38_1.png?raw=true "Title")

---

### Show logedin user info and avatar
![Alt text](images/2021-06-10_15-43.png?raw=true "Title")

---

### Show Modal
![Alt text](images/2021-06-10_15-44.png?raw=true "Title")

---

### Add new record
![Alt text](images/2021-06-10_15-45.png?raw=true "Title")

---

### Edit existing record
![Alt text](images/2021-06-10_15-45_1.png?raw=true "Title")

---

![Alt text](images/2021-06-10_15-47.png?raw=true "Title")

---

### Show pagination
![Alt text](images/2021-06-10_15-53.png?raw=true "Title")

---

![Alt text](images/2021-06-10_15-54.png?raw=true "Title")

---

### Search data from server
![Alt text](images/2021-06-10_16-01.png?raw=true "Title")

---

### Show delete confirmation popup
![Alt text](images/2021-06-10_16-03.png?raw=true "Title")

---

### Hide data from unauthorized user (server side)
![Alt text](images/2021-06-10_16-07.png?raw=true "Title")




## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
