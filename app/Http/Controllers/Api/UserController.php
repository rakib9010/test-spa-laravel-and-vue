<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterUserRequest;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function add(RegisterUserRequest $request)
    {
        $user = User::create($request->input());

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '.' . substr($image->getClientOriginalExtension(), -20);

            /**
             * Shared hodting server not supprted storage symlikn
             */
//            $path = $request->file('image')->storeAs('uploads/image/'.$user->id, $imageName, 'public');

            $path = '/uploads/image/'.$user->id;
            $request->file('image')->move(public_path($path), $imageName);

            $user->image = $path.'/'.$imageName;
            $user->save();
        };


        return response()->json(["success"=>1, 'user'=>$user], 200);
    }
}
