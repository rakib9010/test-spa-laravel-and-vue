<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddRecordRequest;
use App\Http\Requests\DeleteRecordRequest;
use App\Http\Requests\EditRecordRequest;
use App\Models\Record;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class RecorsController extends Controller
{
    public function getRecords(Request $request)
    {
        $search = trim($request->query('search'));
        $records = Record::where('user_id', $request->user()->id);

        if($search!=''){
            $records = $records->where('uuid', 'Like', '%' .$search. '%')
                ->orWhere('name', 'Like', '%' .$search. '%')
                ->orWhere('code', 'Like', '%' .$search)
                ->orWhere('description', 'Like', '%' .$search. '%')
                ;

            if(strtolower($search)=='active'){
                $records = $records->orWhere('status', 1);
            } else if(strtolower($search)=='inactive'){
                $records = $records->orWhere('status', 0);
            }
        }

        $records = $records->orderBy('id', 'desc')->get();

        return response()->json($records, 200);
    }

    public function addRecord(AddRecordRequest $request, Record $record=null)
    {
        $data = $request->input();

        if(empty($data['id'])){
            $record = new Record();
            $record->user_id = $request->user()->id;
            $record->uuid = Uuid::uuid4();
        } else {
            $record = Record::findOrFail($data['id']);
        }

        $record->name = $request->input('name');
        $record->code = $request->input('code');
        $record->description = $request->input('description');
        $record->status = $request->input('status');

        try {
            if($record->save()){
                return response()->json($record, 200);
            } else {
                return response()->json("Failed to save", 500);
            }
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }

    }

    public function editRecord(EditRecordRequest $request, Record $record)
    {
        $record->name = $request->input('name');
        $record->code = $request->input('code');
        $record->description = $request->input('description');
        $record->status = $request->input('status');

        try {
            if($record->save()){
                return response()->json($record, 200);
            } else {
                return response()->json("Failed to save", 500);
            }
        } catch (\Exception $e) {
            return response()->json($e, 500);
        }

    }

    public function deleteRecord(DeleteRecordRequest $request, Record $record)
    {
        $record->delete();
        return response()->json($record, 200);
    }
}
